from django.urls import path
from django.conf.urls import url
from .views import *
app_name = 'homepage'

urlpatterns = [
	url(r"^deleteurl/(?P<delete_id>\d+)/$",delete, name='delete'),
    path('',about, name='about'),
	path('organizations',organizations, name='organizations'),
	path('educations',educations, name='educations'),
	path('experiences',experiences, name='experiences'),
	path('schedule/',schedule, name='schedule'),
    path('schedule/create', schedule_create, name='schedule_create'),
    path('schedule/delete', schedule_delete, name='schedule_delete'),

]